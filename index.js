const http = require('http');

const streznik = http.createServer((req, res)=>{
    console.log('Pišem na izhod...');
    res.writeHead(200, {'Content-type':'text/html'});
    res.end('<html><head><title>Node.js</title></head><body><h1>Node.js</h1><p>Prvi primer na osnovi Node.js</p></body></html>')
});

streznik.listen(4000, ()=>{
    console.log('Strežnik posluša na http://localhost:4000');
});